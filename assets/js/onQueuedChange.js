/**
 * @file
 * Makes queued change functionality.
 */

Drupal.layoutbuilderExtrasLiveUpdateQueuedChange = Drupal.layoutbuilderExtrasLiveUpdateQueuedChange || {};
Drupal.layoutbuilderExtrasLiveUpdateQueuedChange.once = 'queued-change-once';
Drupal.layoutbuilderExtrasLiveUpdateQueuedChange.queuedTime = drupalSettings.layoutbuilder_extras_live_update.queued_time;
(function () {
  Drupal.behaviors.layoutbuilderExtrasLiveUpdateQueuedChange = {
    attach: function () {
      const queuedChangeElements = document.querySelectorAll('[data-queued-change]');
      queuedChangeElements.forEach(function (queuedChangeElement) {
        if (queuedChangeElement.classList.contains(Drupal.layoutbuilderExtrasLiveUpdateQueuedChange.once)) {
          return;
        }
        queuedChangeElement.classList.add(Drupal.layoutbuilderExtrasLiveUpdateQueuedChange.once);

        Drupal.behaviors.layoutbuilderExtrasLiveUpdateQueuedChange.inputEventListener(queuedChangeElement);
      });
    },
    inputEventListener: function(queuedChangeElement) {
      queuedChangeElement.addEventListener('input', function (e) {
        if (e.target.classList.contains('queuing')) {
          return;
        }
        e.target.classList.add('queuing');

        setTimeout(function() {
          const event = new Event('queuedchange');
          e.target.dispatchEvent(event);
          e.target.classList.remove('queuing');
        }, Drupal.layoutbuilderExtrasLiveUpdateQueuedChange.queuedTime);
      });
    },
  };
})();
