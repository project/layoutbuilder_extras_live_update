/**
 * @file
 * Custom changes to Ajax functionality of core.
 */
(function ($) {
  /**
   * Keep the original beforeSend method to use it later.
   */
  let beforeSend = Drupal.Ajax.prototype.beforeSend;

  let closeDialog = Drupal.AjaxCommands.prototype.closeDialog;

  /**
   * Overrides beforeSend to trigger facetblocks update on exposed filter change.
   *
   * @param {XMLHttpRequest} xmlhttprequest
   *   Native Ajax object.
   * @param {object} options
   *   jQuery.ajax options.
   */
  Drupal.Ajax.prototype.beforeSend = function (xmlhttprequest, options) {
    // Call the original Drupal method with the right context.
    beforeSend.apply(this, arguments);

    if (options.extraData && options.extraData._triggering_element_name) {
      const triggeringElement
        = document.querySelector('[name="' + options.extraData._triggering_element_name + '"]');

      if (
          triggeringElement.classList.contains('layoutbuilder-extras-live-update')
      ) {
        triggeringElement.removeAttribute('disabled');
        triggeringElement.focus();
      }
    }
  }

  Drupal.AjaxCommands.prototype.closeDialog = function (ajax, response, status) {
    closeDialog.apply(this, arguments);

    // Reset when closing dialog via Ajax commands.
    drupalSettings.dialog.autoFocus = true;
  };

  // Rest when closing dialog via ESC, close button.
  $(window).on('dialog:beforeclose', function () {
    drupalSettings.dialog.autoFocus = true;
  });

})(jQuery);
