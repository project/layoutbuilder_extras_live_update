/**
 * @file
 * Makes CKEDITOR live updatable.
 */
Drupal.layoutbuilderExtrasLiveUpdateCkeditor = Drupal.layoutbuilderExtrasLiveUpdateCkeditor || {};

Drupal.layoutbuilderExtrasLiveUpdateCkeditor.once = false;

(function () {
  Drupal.behaviors.layoutbuilderExtrasLiveUpdateCkeditor = {
    attach: function () {
      if (Drupal.layoutbuilderExtrasLiveUpdateCkeditor.once) {
        return;
      }
      Drupal.layoutbuilderExtrasLiveUpdateCkeditor.once = true;

      CKEDITOR.on('instanceReady', function () {
        for (var i in CKEDITOR.instances) {
          CKEDITOR.instances[i].on('change', function() {
            Drupal.behaviors.layoutbuilderExtrasLiveUpdateCkeditor.updateHtml(this);
          });
          CKEDITOR.instances[i].on('insertHtml', function() {
            Drupal.behaviors.layoutbuilderExtrasLiveUpdateCkeditor.updateHtml(this);
          });
        }
      });
    },
    updateHtml: function(currentEvent) {
      currentEvent.element.setHtml(currentEvent.getData());
      var event = new Event('input');
      currentEvent.element.$.dispatchEvent(event);
    },
  };
})();

