<?php

namespace Drupal\layoutbuilder_extras_live_update;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\layout_builder\Controller\LayoutRebuildTrait;
use Drupal\layoutbuilder_extras_live_update\Form\LayoutBuilderExtrasLiveUpdateSettingsForm;

/**
 * Callbacks for the ajax live reloading.
 */
class LayoutBuilderExtras {

  use StringTranslationTrait;
  use LayoutRebuildTrait;
  use DependencySerializationTrait;

  /**
   * Saves the form. So that the settings that you change are saved.
   *
   * Because on move HTML gets updates and shit gets lost.
   *
   * @param array $form
   *   Form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An ajax response to maybe change the content.
   */
  public function ajaxSave(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\layoutbuilder_extras\Form\LayoutBuilderExtrasConfigureSectionForm $configureSectionForm */
    $configureSectionForm = $form_state->getBuildInfo()['callback_object'];

    if ($configureSectionForm->isUpdate()) {
      return $this->updateLayoutAndReturnAjaxResponse($form_state, $form);
    }
    else {
      // Return empty response.
      return new AjaxResponse();
    }

  }

  /**
   * Validate and submitted configure section form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $form
   *   The form.
   *
   * @return \Drupal\layout_builder\Form\ConfigureSectionForm
   *   A configure section form that is validated an submitted.
   */
  private function validateAndSubmitConfigureForm(FormStateInterface $form_state, array &$form) {
    /** @var \Drupal\layout_builder\Form\ConfigureSectionForm $configureSectionForm */
    $configureSectionForm = $form_state->getBuildInfo()['callback_object'];
    // Needed for visual change.
    $configureSectionForm->validateForm($form, $form_state);
    $configureSectionForm->submitForm($form, $form_state);
    return $configureSectionForm;
  }

  /**
   * Saves the block form so that the settings/fields that you change are saved.
   *
   * @param array $form
   *   Form object.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An ajax response to maybe change the content.
   */
  public function blockAjaxSave(array &$form, FormStateInterface $form_state) {
    $ajaxResponse = $this->updateLayoutAndReturnAjaxResponse($form_state, $form);

    $ajaxResponse = $this->addHighlightForBlock($ajaxResponse, $form_state);

    return $ajaxResponse;
  }

  /**
   * Makes sure the block gets highlighted again after re-rendering.
   *
   * @param \Drupal\Core\Ajax\AjaxResponse $ajaxResponse
   *   The ajax response to alter.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Ajax response to highlight block.
   */
  private function addHighlightForBlock(AjaxResponse $ajaxResponse, FormStateInterface $form_state) {
    $currentBlockUUID = $form_state->getBuildInfo()['args'][3];
    if (empty($currentBlockUUID) || !is_string($currentBlockUUID) || (
        preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/',
          $currentBlockUUID) !== 1)) {

      return $ajaxResponse;
    }

    $selector = '[data-layout-block-uuid="' . $currentBlockUUID . '"]';
    $method = 'addClass';
    $arguments = ['is-layout-builder-highlighted'];
    $ajaxResponse->addCommand(new InvokeCommand($selector, $method, $arguments));

    return $ajaxResponse;
  }

  /**
   * Updates the forms and returns an ajax response to update it.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param array $form
   *   The form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   A new ajax response by rebuild Layout function.
   */
  private function updateLayoutAndReturnAjaxResponse(FormStateInterface $form_state, array $form): AjaxResponse {
    // Needed for change in TPL.
    /** @var \Drupal\layout_builder\SectionStorageInterface $section_storage */
    $section_storage = $form_state->getBuildInfo()['args'][0];
    $this->validateAndSubmitConfigureForm($form_state, $form);

    return $this->rebuildLayout($section_storage);
  }

  /**
   * Live updating of fields inside of Content blocks.
   *
   * @param array $elements
   *   Array of elements of a form to be processed.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   * @param array $form
   *   The form.
   * @param null $elementTypeManager
   *   The elementType maanger.
   * @param null $definitions
   *   Array of element definitions.
   *
   * @return mixed
   *   The form elements.
   */
  public function processBlockForm(array &$elements, FormStateInterface $form_state, &$form, $elementTypeManager = NULL, $definitions = NULL) {
    // @todo is the best way?
    if (empty($elementTypeManager)) {
      /** @var \Drupal\layoutbuilder_extras_live_update\ElementTypeManagerInterface $elementTypeManager */
      $elementTypeManager
        = \Drupal::service('layoutbuilder_extras_live_update.element_type.manager');
      $definitions = $elementTypeManager->getDefinitions();

      $queuedTimeConfig
        = \Drupal::config(LayoutBuilderExtrasLiveUpdateSettingsForm::SETTINGSNAME)
          ->get('queued_time');
      if (empty($queuedTimeConfig)) {
        $queuedTimeConfig = 1500;
      }
      $elements['#attached']['drupalSettings']['layoutbuilder_extras_live_update']['queued_time']
        = $queuedTimeConfig;

      $elements['#attached']['drupalSettings']['dialog']['autoFocus']
        = FALSE;

      $this->uxAlterationsForBlockLiveUpdate($form);
    }

    foreach ($elements as $key => &$element) {
      $isApplicable =
        LayoutBuilderExtrasLiveUpdateHelper::isElementApplicable($key, $element);

      if (is_array($element)) {
        // Call it self again for nesting.
        $this->processBlockForm($element, $form_state, $form, $elementTypeManager, $definitions);
      }

      if ($isApplicable) {
        $isTypeExistant = isset($element['#type']) && isset($definitions[$element['#type']]);

        $type = 'default_element';

        if ($isTypeExistant) {
          $type = $element['#type'];
        }

        $instance = $elementTypeManager->createInstance($type);
        $instance->applyLogic($element);
      }
    }

    return $elements;
  }

  /**
   * Close dialog when pressing close or ...
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An ajax response to close the dialog (sidebar).
   */
  public function closeDialogAjax(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $command = new CloseDialogCommand('#drupal-off-canvas');
    $response->addCommand($command);
    return $response;
  }

  /**
   * Close dialog and revert any changes done.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An ajax response to close dialog and ajax save the resetted bnlock.
   */
  public function cancelDialogAjax(array $form, FormStateInterface $form_state) {
    // Add close dialog.
    $response = $this->closeDialogAjax($form, $form_state);

    // Reset form.
    $this->resetFormValuesToDefaults($form, $form_state);
    \Drupal::moduleHandler()->alter(
      'layoutbuilder_extras_live_update_cancel_form_state', $form_state);

    // Save and rebuild.
    $responseAjaxSave = $this->blockAjaxSave($form, $form_state);
    $response->setCommands($responseAjaxSave->getCommands());
    return $response;
  }

  /**
   * Loops through values and resets them.
   *
   * Warning it could be that some modules or custom alterations aren't reset.
   * Please use the alter hook for that. (could be an event later on)
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  private function resetFormValuesToDefaults($form, FormStateInterface &$form_state) {
    foreach ($form as $key => $element) {
      $isApplicable =
        LayoutBuilderExtrasLiveUpdateHelper::isElementApplicable($key, $element, TRUE, [
          'container',
          'fieldset',
          'button',
          'submit',
        ]);

      if (is_array($element)) {
        $this->resetFormValuesToDefaults($element, $form_state);
      }

      // Here we need an extra check for integer. For for example textarea
      // Then it starts freaking out because we are replacing a 0 array.
      // Not needed in is applicable otherwise text_format
      // liveupdate doesnt work.
      if ($isApplicable
        && !is_int($key)
        && isset($element['#parents'])
        && isset($element['#default_value'])) {
        $form_state->setValue($element['#parents'], $element['#default_value']);
      }
    }
  }

  /**
   * UX Alterations for live updates of blocks.
   *
   * @param array $form
   *   The form.
   */
  private function uxAlterationsForBlockLiveUpdate(&$form) {
    // @TODO can we do dependency injection here?!
    $disableRemoveSubmit
      = \Drupal::config(LayoutBuilderExtrasLiveUpdateSettingsForm::SETTINGSNAME)
        ->get('disable_removal_submit');

    if (empty($disableRemoveSubmit)) {
      // For UX purposes hide submit, since auto submit due to this module.
      // But a revert/cancel button and a close button that
      // closes the settings tray.
      $form['actions']['submit']['#access'] = FALSE;
    }

    $form['actions']['close'] = [
      '#type' => 'button',
      '#title' => $this->t('Close'),
      '#value' => $this->t('Close'),
      '#ajax' => [
        'callback' => [$this, 'closeDialogAjax'],
        'progress' => 'none',
      ],
      '#attributes' => ['class' => ['button', 'button--ok']],
    ];
    $form['actions']['cancel'] = [
      '#type' => 'button',
      '#title' => $this->t('Cancel changes'),
      '#value' => $this->t('Cancel changes'),
      '#ajax' => [
        'callback' => [$this, 'cancelDialogAjax'],
      ],
      '#attributes' => ['class' => ['button', 'button--warning']],
    ];
  }

}
