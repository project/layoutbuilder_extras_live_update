<?php

namespace Drupal\layoutbuilder_extras_live_update\Plugin\LayoutbuilderExtrasLiveUpdate\ElementType;

/**
 * Text field.
 *
 * @ElementType(
 *  id = "textfield",
 *  label = @Translation("Text field"),
 * )
 */
class TextField extends TextArea {}
