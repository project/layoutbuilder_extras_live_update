<?php

namespace Drupal\layoutbuilder_extras_live_update\Plugin\LayoutbuilderExtrasLiveUpdate\ElementType;

use Drupal\layoutbuilder_extras_live_update\ElementTypeParent;

/**
 * Text area.
 *
 * @ElementType(
 *  id = "textarea",
 *  label = @Translation("Text area"),
 * )
 */
class TextArea extends ElementTypeParent {

  /**
   * {@inheritDoc}
   */
  public function applyLogic(&$element) {
    parent::applyLogic($element);

    $element['#attached']['library'][] = 'layoutbuilder_extras_live_update/lblu.queuedChange';

    $element['#attributes']['data-queued-change'] = 'true';
    $element['#ajax']['event'] = 'queuedchange';
  }

}
