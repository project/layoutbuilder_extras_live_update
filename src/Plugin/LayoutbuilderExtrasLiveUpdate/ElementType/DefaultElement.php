<?php

namespace Drupal\layoutbuilder_extras_live_update\Plugin\LayoutbuilderExtrasLiveUpdate\ElementType;

use Drupal\layoutbuilder_extras_live_update\ElementTypeParent;

/**
 * Default element.
 *
 * @ElementType(
 *  id = "default_element",
 *  label = @Translation("Default element"),
 * )
 */
class DefaultElement extends ElementTypeParent {

}
