<?php

namespace Drupal\layoutbuilder_extras_live_update\Plugin\LayoutbuilderExtrasLiveUpdate\ElementType;

use Drupal\layoutbuilder_extras_live_update\ElementTypeParent;

/**
 * Text format.
 *
 * @ElementType(
 *  id = "text_format",
 *  label = @Translation("Text format"),
 * )
 */
class TextFormat extends ElementTypeParent {

  /**
   * {@inheritDoc}
   */
  public function applyLogic(&$element) {
    parent::applyLogic($element);

    $element['#attributes']['data-queued-change'] = 'true';
    $element['#attached']['library'][] = 'layoutbuilder_extras_live_update/lblu.ckeditora';
    $element['#attached']['library'][] = 'layoutbuilder_extras_live_update/lblu.queuedChange';
    $element['#ajax']['event'] = 'queuedchange';
  }

}
