<?php

namespace Drupal\layoutbuilder_extras_live_update\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\layoutbuilder_extras_live_update\LayoutBuilderExtras;
use Drupal\media_library\Plugin\Field\FieldWidget\MediaLibraryWidget;

/**
 * Plugin implementation of the 'media_library_widget' widget.
 *
 * @FieldWidget(
 *   id = "lb_extras_live_update_media_library_widget",
 *   label = @Translation("LB Extras - Live update - Media library"),
 *   description = @Translation("LB Extras - Live update - Allows you to select items from the media library."),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   multiple_values = TRUE,
 * )
 *
 * @internal
 *   Plugin classes are internal.
 */
class LBExtrasLiveUpdateMediaLibraryWidget extends MediaLibraryWidget {

  /**
   * AJAX callback to update the widget when the selection changes.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   An AJAX response to update the selection.
   */
  public static function updateWidget(array $form, FormStateInterface $form_state) {
    $response = parent::updateWidget($form, $form_state);

    // If it's a new block, no need to live update...nothing.
    if ($form['#id'] === 'layout-builder-add-block') {
      return $response;
    }

    $route = \Drupal::routeMatch()->getRouteObject();
    if ($route === NULL) {
      return $response;
    }

    $lbRequirement = $route->getRequirement('_layout_builder_access');
    $isAdminRoute = $route->getOption('_admin_route');

    if (!empty($lbRequirement) && $isAdminRoute) {
      $lb = new LayoutBuilderExtras();
      $responseLbExtraLiveUpdates = $lb->blockAjaxSave($form, $form_state);
      $commandsLiveUpdate = $responseLbExtraLiveUpdates->getCommands();
      $response->setCommands($commandsLiveUpdate);
    }

    return $response;
  }

}
