<?php

namespace Drupal\layoutbuilder_extras_live_update;

/**
 * Provides an element Type interface.
 */
interface ElementTypeInterface {

  /**
   * Applies logic/code to the given element.
   *
   * @param array $element
   *   The element to act upon.
   */
  public function applyLogic(&$element);

}
