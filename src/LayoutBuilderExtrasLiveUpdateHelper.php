<?php

namespace Drupal\layoutbuilder_extras_live_update;

/**
 * Helper.
 */
class LayoutBuilderExtrasLiveUpdateHelper {

  /**
   * Should we attach to this element or not.
   *
   * @return bool
   *   if True we attach live update to the element.
   */
  public static function isElementApplicable($key,
                                             $element,
                                             $skipAjaxCheck = FALSE,
                                             $skippedElements = [
                                               'container',
                                               'fieldset',
                                               'button',
                                               'submit',
                                               'hidden',
                                             ]) {
    // Skip reserved form elements.
    if (str_contains($key, '#') || !is_array($element)) {
      return FALSE;
    }

    // Is it a "real" FAPI element?. Dumb check ofc.
    if (!isset($element['#type'])) {
      return FALSE;
    }

    // Check for empty to not override existing ajax callbacks like on MEdia.
    // For example the updateWidgt.
    if (!$skipAjaxCheck && !empty($element['#ajax']['callback'])) {
      return FALSE;
    }

    // Extra skips.
    if (in_array($key, [
      'form_token',
      'weight',
    ])) {
      return FALSE;
    }

    // Extra cleans.
    if (in_array($element['#type'], $skippedElements)) {
      return FALSE;
    }

    return TRUE;
  }

}
