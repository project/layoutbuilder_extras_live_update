<?php

namespace Drupal\layoutbuilder_extras_live_update;

use Drupal\Component\Plugin\Discovery\CachedDiscoveryInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;

/**
 * Provides an element type plugin manager interface.
 *
 * @see plugin_api
 */
interface ElementTypeManagerInterface extends PluginManagerInterface, CachedDiscoveryInterface, CacheableDependencyInterface {


}
