<?php

namespace Drupal\layoutbuilder_extras_live_update\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form.
 */
class LayoutBuilderExtrasLiveUpdateSettingsForm extends ConfigFormBase {

  const SETTINGSNAME = 'layoutbuilder_extras_live_update.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'layoutbuilder_extras_live_update';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::SETTINGSNAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config(self::SETTINGSNAME);

    $form['queued_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Queued time'),
      '#description' => $this->t('The time in seconds to wait for live updates are rendered. Only used on textfields, textarea currently.'),
      '#default_value' => $config->get('queued_time') ?? 1500,
    ];

    $form['live_update_fields'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Live update of fields'),
      '#description' => $this->t('Do you want live updating of fields in blocks? Like for example when typing in a textfield/textarea the text automatically updates in the frontend. This is related to Queued time setting.'),
      '#default_value' => $config->get('live_update_fields') ?? TRUE,
    ];

    $form['disable_removal_submit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable the UX improvement to remove the submit button'),
      '#description' => $this->t('Layout builder extras tries to update everthing, but in some cases like currently with EntityBrowser it is not supported. This bypasses the removal of that submit button so it still works.'),
      '#default_value' => $config->get('disable_removal_submit') ?? TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->configFactory->getEditable(self::SETTINGSNAME);

    $config->set('queued_time', $form_state->getValue('queued_time', FALSE));
    $config->set('disable_removal_submit', $form_state->getValue('disable_removal_submit', TRUE));
    $config->set('live_update_fields', $form_state->getValue('live_update_fields', TRUE));

    $config->save();
  }

}
