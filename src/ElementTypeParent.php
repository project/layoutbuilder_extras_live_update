<?php

namespace Drupal\layoutbuilder_extras_live_update;

/**
 * Parent class for Element Types.
 */
class ElementTypeParent implements ElementTypeInterface {

  /**
   * {@inheritDoc}
   */
  public function applyLogic(&$element) {
    $element['#ajax']['callback'] = [new LayoutBuilderExtras(), 'blockAjaxSave'];
    // The AJAX system automatically moves focus to the first tabbable
    // element of the modal, so we need to disable refocus on the button.
    $element['#ajax']['disable-refocus'] = FALSE;

    $element['#ajax']['progress']['type'] = 'none';

    // Used for testing/debugging.
    $element['#attributes']['class'][] = 'layoutbuilder-extras-live-update';

    $element['#attached']['library'][] = 'layoutbuilder_extras_live_update/lblu.ajaxChanges';
  }

}
