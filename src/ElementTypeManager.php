<?php

namespace Drupal\layoutbuilder_extras_live_update;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides an ElementType plugin manager.
 *
 * @see plugin_api
 */
class ElementTypeManager extends DefaultPluginManager implements ElementTypeManagerInterface {

  /**
   * Constructs a ElementType object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/LayoutbuilderExtrasLiveUpdate/ElementType',
      $namespaces,
      $module_handler,
      'Drupal\layoutbuilder_extras_live_update\ElementTypeInterface',
      'Drupal\layoutbuilder_extras_live_update\Annotation\ElementType'
    );
    $this->alterInfo('layoutbuilder_extras_live_update_element_type_info');
    $this->setCacheBackend($cache_backend, 'layoutbuilder_extras_live_update_element_type_info_plugins');
  }

}
