<?php

namespace Drupal\layoutbuilder_extras_live_update\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a reusable element type plugin annotation object.
 *
 * @Annotation
 */
class ElementType extends Plugin {}
