## What does it do

When you change a setting on a Layout Builder section, for example Color, background, .... this will update the layout immediately so you can see your changes without pressing the save button.

## Documentation
Just install and you should be good to go.

## Note
Only works for radios, radio, select, checkbox and checkboxes currently. Could be expanded if needed.
